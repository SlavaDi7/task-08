package com.epam.rd.java.basic.task8;

import java.util.Comparator;

public class SoilComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.soil.compareTo(o2.soil);
    }
}
