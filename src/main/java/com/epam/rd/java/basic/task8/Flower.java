package com.epam.rd.java.basic.task8;


public class Flower {

    public String name;
    public Soil soil;
    public String origin;
    public VisualParameters visualParameters;
    public GrowingTips growingTips;
    public Multiplying multiplying;

    @Override
    public String toString() {
        String result = null;
        result = "name - " + name +
                "\nsoil - " + soil + " " + soil.getSoil() +
                "\norigin - " + origin +
                "\n" + visualParameters +
                "\n" + growingTips +
                "\nmultiplying - "  +  multiplying + " " + multiplying.getMultiplying();
        return result;
    }

    public enum Soil {
        PODZOLIC("подзолистая"), GROUND("грунтовая"), SOD_PODZOLIC("дерново-подзолистая");
        private String soil;

        public static Soil getSoilByName(String name){
            switch (name) {
                case ("подзолистая") : return Soil.PODZOLIC;
                case ("грунтовая") : return Soil.GROUND;
                case ("дерново-подзолистая") : return Soil.SOD_PODZOLIC;
                default: return null;
            }
        }

        Soil(String soil) {
            this.soil = soil;
        }

        public String getSoil() {
            return soil;
        }
    }

    public static class VisualParameters {
        public String stemColour;
        public String leafColor;
        public String aveLenFlower;
        public String aveLenFlowerMeasure;

        @Override
        public String toString() {
            return "Visual parameters:" +
                    "\n\t stemColour " + stemColour +
                    "\n\t leafColor " + leafColor +
                    "\n\t aveLenFlower " + aveLenFlower + " " +
                    aveLenFlowerMeasure;
        }
    }

        public static class GrowingTips {
            public String temperature;
            public String tempMeasure;
            public String lighting;
            public String watering;
            public String wateringMeasure;

            public GrowingTips() {
            }

            public GrowingTips(String temperature, String lighting, String watering) {
                this.temperature = temperature;
                this.lighting = lighting;
                this.watering = watering;
            }

            @Override
            public String toString() {
                return "Growing tips:" +
                        "\n\t temperature " + temperature + " " + tempMeasure +
                        "\n\t lighting " + lighting +
                        "\n\t watering " + watering + " " + wateringMeasure;
            }
        }

        public enum Multiplying {
            LEAFS("листья"), STALKS("черенки"), SEEDS("семена");
            private String multiplying;

            public static Multiplying getMultiplyingByName(String name){
                switch (name) {
                    case ("листья") : return Multiplying.LEAFS;
                    case ("черенки") : return Multiplying.STALKS;
                    case ("семена") : return Multiplying.SEEDS;
                    default: return null;
                }
            }


            Multiplying(String multiplying) {
                this.multiplying = multiplying;
            }

            public String getMultiplying() {
                return multiplying;
            }


        }

    }
