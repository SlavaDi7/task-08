package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;


/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	//private String outputXMLFileName = "output.dom.xml";

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseDom() throws ParserConfigurationException {
		Flowers flowers= new Flowers();
		DocumentBuilder docBuilder;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl",true);
		factory.setNamespaceAware(true);
		try{
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e){
			e.printStackTrace();
			return null;
		}
		Document doc;
		try {
			//new FileInputStream(new File("XML.xml"));
			doc = docBuilder.parse(new FileInputStream(new File(xmlFileName)), "UTF-8");
			//doc = docBuilder.parse(xmlFileName);
			Element root = doc.getDocumentElement();
				// getting a list of <flowers> child elements
			NodeList nodeList = root.getElementsByTagName("flower");
			//doc.getDocumentElement().normalize();
			for (int i = 0; i < nodeList.getLength(); i++){
				Element flowerElement = (Element)nodeList.item(i);
				Flower flower = buildFlower(flowerElement);
				flowers.getFlowers().add(flower);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return flowers;
	}

	private Flower buildFlower(Element flowerElement){
		Flower flower = new Flower();

		//flower.name = flowerElement.getAttribute("name");
		flower.name = flowerElement.getChildNodes().item(1).getTextContent();
		flower.origin = getElementTextContent(flowerElement, "origin");
		//flower.origin = flowerElement.getChildNodes().item(5).getTextContent();

		flower.growingTips = new Flower.GrowingTips();
		Element growingTipsElement = (Element) flowerElement.getElementsByTagName("growingTips").item(0);
		flower.growingTips.temperature = getElementTextContent(growingTipsElement,"tempreture");
		flower.growingTips.tempMeasure = ((Element)growingTipsElement.getElementsByTagName("tempreture").item(0)).
				getAttribute("measure");
		//flower.growingTips.lighting = getElementTextContent(growingTipsElement,"lighting");
		flower.growingTips.lighting = ((Element)growingTipsElement.getElementsByTagName("lighting").item(0)).
					getAttribute("lightRequiring");
		flower.growingTips.watering = getElementTextContent(growingTipsElement,"watering");
		flower.growingTips.wateringMeasure = ((Element)growingTipsElement.getElementsByTagName("watering").item(0)).
				getAttribute("measure");

		flower.visualParameters = new Flower.VisualParameters();
		Element visualParametersElement = (Element) flowerElement.getElementsByTagName("visualParameters").item(0);
		flower.visualParameters.aveLenFlower = getElementTextContent(visualParametersElement, "aveLenFlower");
		flower.visualParameters.aveLenFlowerMeasure = ((Element)visualParametersElement.getElementsByTagName("aveLenFlower").item(0)).
				getAttribute("measure");
		flower.visualParameters.leafColor = getElementTextContent(visualParametersElement,"leafColour");
		flower.visualParameters.stemColour = getElementTextContent(visualParametersElement,"stemColour");

		//System.out.println("attr=" + flowerElement.getAttribute("multiplying"));
		flower.multiplying = Flower.Multiplying.getMultiplyingByName(getElementTextContent(flowerElement,"multiplying"));
		flower.soil =  Flower.Soil.getSoilByName(getElementTextContent(flowerElement,"soil"));
		//System.out.println(flower);
		return flower;
	}

	private static String getElementTextContent(Element element, String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}

	public void createXML (Flowers flowers, String outputXMLFileName) {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = null;
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		//forming a doc tree
		Document document = documentBuilder.newDocument();
		String root = "flowers";
		Element rootElement = document.createElement(root);
		rootElement.setAttribute("xmlns","http://www.nure.ua");
		rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");
		document.appendChild(rootElement);
		for (Flower f : flowers.getFlowers()) {
			Element elementFlower = document.createElement("flower");
			Element elementName = document.createElement("name");
			Element elementSoil = document.createElement("soil");
			Element elementOrigin = document.createElement("origin");
			elementName.appendChild(document.createTextNode(f.name));
			elementSoil.appendChild(document.createTextNode(f.soil.getSoil()));
			elementOrigin.appendChild(document.createTextNode(f.origin));

			elementFlower.appendChild(elementName);
			elementFlower.appendChild(elementSoil);
			elementFlower.appendChild(elementOrigin);

			Element elementVisualParameters = document.createElement("visualParameters");

			Element elementStemColour = document.createElement("stemColour");
			elementStemColour.appendChild(document.createTextNode(f.visualParameters.stemColour));
			elementVisualParameters.appendChild(elementStemColour);

			Element elementLeafColour = document.createElement("leafColour");
			elementLeafColour.appendChild(document.createTextNode(f.visualParameters.leafColor));
			elementVisualParameters.appendChild(elementLeafColour);

			Element elementAveLenFlower = document.createElement("aveLenFlower");
			elementAveLenFlower.appendChild(document.createTextNode(f.visualParameters.aveLenFlower));
			elementAveLenFlower.setAttribute("measure", f.visualParameters.aveLenFlowerMeasure);
			elementVisualParameters.appendChild(elementAveLenFlower);

			Element elementGrowingTips = document.createElement("growingTips");

			Element elementTempreture = document.createElement("tempreture");
			elementTempreture.appendChild(document.createTextNode(f.growingTips.temperature));
			elementTempreture.setAttribute("measure", f.growingTips.tempMeasure);
			elementGrowingTips.appendChild(elementTempreture);

			Element elementLighting = document.createElement("lighting");
			elementLighting.setAttribute("lightRequiring", f.growingTips.lighting);
			elementGrowingTips.appendChild(elementLighting);

			Element elementWatering = document.createElement("watering");
			elementWatering.appendChild(document.createTextNode(f.growingTips.watering));
			elementWatering.setAttribute("measure", f.growingTips.wateringMeasure);
			elementGrowingTips.appendChild(elementWatering);

			Element elementMultiplying = document.createElement("multiplying");
			elementMultiplying.appendChild(document.createTextNode(f.multiplying.getMultiplying()));

			elementFlower.appendChild(elementVisualParameters);
			elementFlower.appendChild(elementGrowingTips);
			elementFlower.appendChild(elementMultiplying);

			rootElement.appendChild(elementFlower);
		}
		//document.normalize();
		//write tree to file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new FileWriter(outputXMLFileName));
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException | IOException e) {
			e.printStackTrace();
		}

		// PLACE YOUR CODE HERE
	}
}
