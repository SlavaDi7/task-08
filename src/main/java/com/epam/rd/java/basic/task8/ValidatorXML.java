package com.epam.rd.java.basic.task8;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class ValidatorXML {

    private String schemaName = "input.xsd";
    private static String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
    private SchemaFactory schemaFactory;

    public void Validate (String xmlFileName, String schemaName) throws SAXException {
        File schemaLocation = new File(schemaName);
        Source source = new StreamSource(xmlFileName);
        try{
            schemaFactory = SchemaFactory.newInstance(language);
            Schema schema = schemaFactory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            //document check
            validator.setErrorHandler(new ErrorHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    System.err.println(exception.getMessage());
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    System.err.println(exception.getMessage());
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    System.err.println(exception.getMessage());
                }
            });

            validator.validate(source);
            System.out.println(source.getSystemId() + " validation success.");
        } catch (SAXException e) {
            System.err.println(xmlFileName + " is not valid " + e.getMessage() );
            throw new SAXException(source.getSystemId() + "is not valid, reason ", e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
