package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.util.Locale;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private static final String ELEMENT_FLOWER = "flower";
	private static final String ELEMENT_VISUAL_PARAMETERS = "visualParameters";
	private static final String ELEMENT_GROWING_TIPS = "growingTips";

	
	private String xmlFileName;

	private String schemaName = "input.xsd";
	private static String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
	private SchemaFactory schemaFactory = SchemaFactory.newInstance(language);

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Flower currentFlower;
	private Flowers flowers ;
	private FlowerXMLTag currentXMLTag;

	private XMLReader reader;

	public Flowers parseSax(){
		try {
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser parser = saxParserFactory.newSAXParser();
			reader = parser.getXMLReader();
			reader.setContentHandler(this);
			reader.parse(xmlFileName);
		} catch (SAXException| IOException e){
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e){
			e.printStackTrace();
			return null;
		}
		//System.out.println(flowers);
		return flowers;
	}

	public void createSAX(Flowers flowers, String output){

	}


	@Override
	public void startDocument(){
		//System.out.println("startDocument");
		flowers = new Flowers();
	}
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr) {
		//System.out.println("startElement " + qName);
		if (ELEMENT_FLOWER.equals(qName)){
			currentFlower = new Flower();
			currentFlower.visualParameters = new Flower.VisualParameters();
			currentFlower.growingTips = new Flower.GrowingTips();
			return;
		} else {
			FlowerXMLTag temp = FlowerXMLTag.valueOf(qName.toUpperCase());
			currentXMLTag = temp;
			//System.out.println("currentXMLTag= " + temp);

			if (currentXMLTag != null && attr.getLength() != 0) {
				switch (currentXMLTag){
					case NAME : break;
					case SOIL: 	break;
					case ORIGIN:  break;
					case STEMCOLOUR:  break;
					case LEAFCOLOUR:  break;
					case AVELENFLOWER: currentFlower.visualParameters.aveLenFlowerMeasure = attr.getValue(0); break;
					case TEMPRETURE: currentFlower.growingTips.tempMeasure = attr.getValue(0); break;
					case LIGHTING: currentFlower.growingTips.lighting = attr.getValue(0); break;
					case WATERING: currentFlower.growingTips.wateringMeasure = attr.getValue(0); break;
					case MULTIPLYING: break;
					case FLOWERS: break;
					case VISUALPARAMETERS:break;
					case GROWINGTIPS: break;
					default: throw new EnumConstantNotPresentException(currentXMLTag.getDeclaringClass(), currentXMLTag.name());

				}
			}

		}


	}
	@Override
	public void endElement(String uri, String localName, String qName){
		if (ELEMENT_FLOWER.equals(qName)) {
			flowers.getFlowers().add(currentFlower);
			//System.out.println("added flower");
			//System.out.println(currentFlower);
		}

	}

	@Override
	public void characters(char ch[], int start, int length){
		String data = new String(ch, start, length).strip();
		//System.out.println("characters= " + data);
		if (currentXMLTag != null && !data.isEmpty()) {
			switch (currentXMLTag){
				case NAME : {
					currentFlower.name = data;
					//System.out.println("currFlname= " + currentFlower.name);
					break;}
				case SOIL: {
					currentFlower.soil = Flower.Soil.getSoilByName(data);
					//System.out.println("currentFlowerSoil= " + currentFlower.soil);
					break;}
				case ORIGIN: currentFlower.origin = data; break;
				case STEMCOLOUR: currentFlower.visualParameters.stemColour = data; break;
				case LEAFCOLOUR: currentFlower.visualParameters.leafColor = data; break;
				case AVELENFLOWER: currentFlower.visualParameters.aveLenFlower = data; break;
				case TEMPRETURE: currentFlower.growingTips.temperature = data; break;
				case LIGHTING: currentFlower.growingTips.lighting = data; break;
				case WATERING: currentFlower.growingTips.watering = data; break;
				case MULTIPLYING: currentFlower.multiplying = Flower.Multiplying.getMultiplyingByName(data); break;
				case FLOWERS: break;
				case VISUALPARAMETERS:break;
				case GROWINGTIPS: break;
				default: throw new EnumConstantNotPresentException(currentXMLTag.getDeclaringClass(), currentXMLTag.name());

			}
		}

	}

	private enum FlowerXMLTag {
		FLOWERS("flowers"),
		FLOWER("flower"),
		NAME("name"),
		SOIL("soil"),
		ORIGIN("origin"),
		STEMCOLOUR("stemColour"),
		LEAFCOLOUR("leafColour"),
		AVELENFLOWER("aveLenFlower"),
		TEMPRETURE("temperature"),
		LIGHTING("lighting"),
		WATERING("watering"),
		MULTIPLYING("multiplying"),
		VISUALPARAMETERS("visualParameters"),
		GROWINGTIPS ("growingTips");

		private String value;
		FlowerXMLTag(String value){
			this.value = value;
		}
		private String getValue(){
			return value;
		}
	}

	// PLACE YOUR CODE HERE

}