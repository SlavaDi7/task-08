package com.epam.rd.java.basic.task8;

import java.util.Comparator;

public class NameComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.name.compareTo(o2.name);
    }
}
