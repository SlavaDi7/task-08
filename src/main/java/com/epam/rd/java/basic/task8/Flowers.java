package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers(){
        return flowers;
    }

    @Override
    public String toString(){
        StringBuilder strBuilder = new StringBuilder();
        for (Flower f : flowers){
            strBuilder.append(f).append(System.lineSeparator()).append("------").append(System.lineSeparator());
        }
        return strBuilder.toString();
    }

}
