package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		ValidatorXML validator = new ValidatorXML();
		validator.Validate(xmlFileName, "input.xsd");
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

		Flowers flowers = domController.parseDom();
		//System.out.println(flowers);

		// sort (case 1)
		flowers.getFlowers().sort(new NameComparator());
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.createXML(flowers, outputXmlFile);
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = saxController.parseSax();

		// sort  (case 2)
		flowers.getFlowers().sort(new OriginComparator());
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		domController.createXML(flowers, outputXmlFile);
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = staxController.parseStax();
		System.out.println(flowers);
		// sort  (case 3)
		flowers.getFlowers().sort(new SoilComparator());
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		domController.createXML(flowers, outputXmlFile);
		// PLACE YOUR CODE HERE
	}

}
