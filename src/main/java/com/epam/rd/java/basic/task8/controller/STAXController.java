package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	//private Flowers flowers;


	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseStax(){
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader;
		String name = null;
		Flowers flowers = new Flowers();
		try (FileInputStream inputStream = new FileInputStream(new File(xmlFileName))){
			reader = inputFactory.createXMLStreamReader(inputStream);
			//parsing
			while (reader.hasNext()){
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT){
					name = reader.getLocalName();
					//System.out.println("name= " + name);
					if (name.equals(FlowerXMLTag.FLOWER.getValue())){
						Flower flower = buildFlower(reader);
						flowers.getFlowers().add(flower);
					}
				}
			}

		} catch (IOException e){
			e.printStackTrace(); return null;
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		return flowers;
	}

	private Flower buildFlower(XMLStreamReader reader) throws XMLStreamException{
		Flower flower = new Flower();
		String name;
		while(reader.hasNext()){
			int type = reader.next();
			switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					switch (FlowerXMLTag.valueOf(name.toUpperCase())){
						case NAME: flower.name = getXMLText(reader);break;
						case SOIL: flower.soil = Flower.Soil.getSoilByName(getXMLText(reader));break;
						case ORIGIN: flower.origin = getXMLText(reader); break;
						case MULTIPLYING: flower.multiplying = Flower.Multiplying.getMultiplyingByName(getXMLText(reader)); break;
						case VISUALPARAMETERS: flower.visualParameters = getXMLVisualParameters(reader); break;
						case GROWINGTIPS: flower.growingTips = getXMLGrowingTips(reader);break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (FlowerXMLTag.valueOf(name.toUpperCase()) == FlowerXMLTag.FLOWER){
						return flower;
					}
					break;
			}
		}
		return flower;
	}

	private Flower.VisualParameters getXMLVisualParameters(XMLStreamReader reader)throws XMLStreamException {
		Flower.VisualParameters visualParameters = new Flower.VisualParameters();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch(type){
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					//if (FlowerXMLTag.valueOf(name.toUpperCase()) == FlowerXMLTag.AVELENFLOWER) {
						visualParameters.aveLenFlowerMeasure = reader.getAttributeValue(null, FlowerXMLTag.AVELENFLOWERMEASURE.getValue());
					//}
					switch (FlowerXMLTag.valueOf(name.toUpperCase())){
						case STEMCOLOUR : visualParameters.stemColour = getXMLText(reader); break;
						case LEAFCOLOUR: visualParameters.leafColor = getXMLText(reader); break;
						case AVELENFLOWER:
							visualParameters.aveLenFlowerMeasure = reader.getAttributeValue(null, FlowerXMLTag.AVELENFLOWERMEASURE.getValue());
							visualParameters.aveLenFlower = getXMLText((reader));
							break;
					}
					break;

				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (FlowerXMLTag.valueOf(name.toUpperCase()) == FlowerXMLTag.VISUALPARAMETERS) { return visualParameters; }
					break;
			}

		}
		return null;
	}

	private Flower.GrowingTips getXMLGrowingTips (XMLStreamReader reader) throws XMLStreamException {
		Flower.GrowingTips growingTips = new Flower.GrowingTips();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch(type){
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					//if (FlowerXMLTag.valueOf(name.toUpperCase()) == FlowerXMLTag.AVELENFLOWER) {
					//visualParameters.aveLenFlowerMeasure = reader.getAttributeValue(null, FlowerXMLTag.AVELENFLOWERMEASURE.getValue());
					//}
					switch (FlowerXMLTag.valueOf(name.toUpperCase())){
						case TEMPRETURE:
							growingTips.tempMeasure = reader.getAttributeValue(null, FlowerXMLTag.TEMPMEASURE.getValue());
							growingTips.temperature = getXMLText(reader);
							break;
						case LIGHTING:
							growingTips.lighting = reader.getAttributeValue(null, FlowerXMLTag.AVELENFLOWERMEASURE.getValue());
							break;
						case WATERING:
							growingTips.wateringMeasure = reader.getAttributeValue(null, FlowerXMLTag.WATERINGMEASURE.getValue());
							growingTips.watering = getXMLText((reader));
							break;
					}
					break;

				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (FlowerXMLTag.valueOf(name.toUpperCase()) == FlowerXMLTag.GROWINGTIPS) { return growingTips; }
					break;
			}

		}

		return null;
	}

	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
				String text = null;
				if (reader.hasNext()){
					reader.next();
					text = reader.getText();
				}
		return text;
	}

	private enum FlowerXMLTag{

		FLOWERS("flowers"),
		FLOWER("flower"),
		NAME("name"),
		SOIL("soil"),
		ORIGIN("origin"),
		STEMCOLOUR("stemColour"),
		LEAFCOLOUR("leafColour"),
		AVELENFLOWER("aveLenFlower"),
		AVELENFLOWERMEASURE("measure"),
		TEMPRETURE("temperature"),
		TEMPMEASURE("measure"),
		LIGHTING("lighting"),
		WATERING("watering"),
		WATERINGMEASURE("measure"),
		MULTIPLYING("multiplying"),
		VISUALPARAMETERS("visualParameters"),
		GROWINGTIPS ("growingTips");

		private String value;
		FlowerXMLTag(String value){ this.value = value; }
		public String getValue() { return value; }
	}
	// PLACE YOUR CODE HERE

}